<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marcadores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 */
class Marcadores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marcadores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre','descripcion','tipo','url'], 'string', 'max' => 255],
            [['descripcionl'], 'string', 'max' => 600],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'descripcionl'=> 'Descripcion Larga',
            'tipo'=> 'Tipo',
            'url'=> 'Url'
        ];
    }

    /**
     * @inheritdoc
     * @return MarcadoresQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MarcadoresQuery(get_called_class());
    }
}
