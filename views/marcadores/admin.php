<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="marcadores-index">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'nombre',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a($model->nombre, ['marcadores/view', 'id' => $model->id], ['class' => 'profile-link']);
                },
            ],
            [
                'attribute' => 'descripcion',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a($model->descripcion, ['marcadores/index', 'id' => $model->id], ['class' => 'profile-link']);
                },
            ],
        ],
        'summary' => false,
    ]);
    ?>

</div>

<p>
    <?= Html::a('Nuevo',['create'],['class' =>'btn btn-success col-lg-offset-11'])?>
</p>


