<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="marcadores-index">



    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'nombre',
                'format' => 'html',
                'value' => function ($model) {
                    return '<a href="' . $model->url . '">' . $model->nombre . '</a>';
                },
            ],
            [
                'attribute' => 'descripcion',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a($model->descripcion, ['marcadores/index', 'id' => $model->id], ['class' => 'profile-link']);
                },
            ],
        ],
        'summary' => false,
    ]);
    ?>
<!-- Otra forma de mostrar el listado con ListView
    <div class="row table-bordered listado">
    <?
     echo   ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_registro',
            'summary' => false
        ]);
        ?>
   </div>-->


</div>

<?php
if($datos){
        Modal::begin([
                        'header' => '<h2>Descripcion del Enlace </h2>',
//                        'toggleButton' => [
//                            'label' => $datos->descripcion,
//                            'class' => 'btn'
//                        ],
                    'clientOptions'=>['show'=>'true'],
                    ]);

                    echo $datos->descripcionl;
?>
<div class="row">
    <div class="col-lg-offset-8 col-lg-2">
<a href="<?=$datos->url?>" class="btn btn-info" target="_blank">Visitar</a>
</div>
<div class="col-lg-2">
<?= Html::a("Cerrar", ['marcadores/index'], ['class' => 'btn btn-info'])?>

</div></div>
<?php
    Modal::end();
}
?>

