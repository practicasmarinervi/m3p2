<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Marcadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marcadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'descripcionl')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'tipo')->dropDownList(['publico' => 'publico', 'privado'=>'privado']) ?>
    
    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success col-lg-offset-10']) ?>
         <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro que quieres eliminar este elemento?',
                'method' => 'post',
            ]])?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
