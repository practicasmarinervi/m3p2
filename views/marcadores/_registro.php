<?php
use yii\bootstrap\Modal;
?>

<div class="celdas col-lg-4">
    <?=$model->id ?>
</div>
<div class="celdas col-lg-4">
    <a href="<?=$model->url?>">
        <?= $model->descripcion ?>
    </a>
</div>
<div class="celdas col-lg-4">
<?php
    Modal::begin([
    'header' => '<h2>Descripcion del Enlace </h2>',
    'toggleButton' => [
        'label' => $model->descripcion,
        'class' => 'btn'
    ],
]);

echo $model->descripcionl;

Modal::end();
?>
</div>
